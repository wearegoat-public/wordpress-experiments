This repository is used to research and experiment with WordPress.

## Prerequisites

- [MAMP](https://www.mamp.info/en/)
- MAMP Web Server Document Root (found in Preferences) is using the root folder in this repository.

## How to run it

1. Open the MAMP application
2. Click "Start Servers" in the MAMP application
3. Navigate to:
   - the php admin page through localhost:8888/phpMyAdmin
   - the website through localhost:8888/wordpress
   - the WordPress editor through localhost:8888/wordpress/wp-admin

## Deployment

Deployment can be done by simply pushing the master branch to remote. Any changes applied to the branch will be deployed to WordPress through [WP Pusher](https://wppusher.com/).
